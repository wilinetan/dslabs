package dslabs.primarybackup;

import com.google.common.base.Objects;
import dslabs.atmostonce.AMOCommand;
import dslabs.atmostonce.AMOResult;
import dslabs.framework.Address;
import dslabs.framework.Client;
import dslabs.framework.Command;
import dslabs.framework.Node;
import dslabs.framework.Result;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static dslabs.primarybackup.ClientTimer.CLIENT_RETRY_MILLIS;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
class PBClient extends Node implements Client {
    private final Address viewServer;

    // Your code here...
    private static final int MAXIMUM_RETRIES = 3;
    private Address primaryServer;
    private int currSeq = 0;
    private int retryCount = 0;
    private AMOCommand amoCommand;
    private AMOResult amoResult;
    /* -------------------------------------------------------------------------
        Construction and Initialization
       -----------------------------------------------------------------------*/
    public PBClient(Address address, Address viewServer) {
        super(address);
        this.viewServer = viewServer;
    }

    @Override
    public synchronized void init() {
        // Your code here...
        // Poll view server on who primary is on startup
        send(new GetView(), viewServer);
    }

    /* -------------------------------------------------------------------------
        Client Methods
       -----------------------------------------------------------------------*/
    @Override
    public synchronized void sendCommand(Command command) {
        // Your code here...
        AMOCommand amoCommand = new AMOCommand(command, address(), currSeq);
        Request request = new Request(amoCommand);
        this.amoCommand = amoCommand;
        amoResult = null;

        set(new ClientTimer(request), CLIENT_RETRY_MILLIS);
        if (primaryServer == null) return;

        send(request, primaryServer);
    }

    @Override
    public synchronized boolean hasResult() {
        // Your code here...
        return amoResult != null;
    }

    @Override
    public synchronized Result getResult() throws InterruptedException {
        // Your code here...
        while (amoResult == null) {
            wait();
        }
        return amoResult.result();
    }

    /* -------------------------------------------------------------------------
        Message Handlers
       -----------------------------------------------------------------------*/
    private synchronized void handleReply(Reply m, Address sender) {
        // Your code here...
        if (Objects.equal(m.result().sequenceNum(), currSeq) && amoResult == null) {
            currSeq++;
            amoResult = m.result();
            notify();
        }
    }

    private synchronized void handleViewReply(ViewReply m, Address sender) {
        // Your code here...
        primaryServer = m.view().primary();
    }

    // Your code here...

    /* -------------------------------------------------------------------------
        Timer Handlers
       -----------------------------------------------------------------------*/
    private synchronized void onClientTimer(ClientTimer t) {
        // Your code here...
        AMOCommand timerCommand = t.request().command();
        if (amoCommand.sequenceNum() == timerCommand.sequenceNum()
                && amoResult == null) {
            retryCount++;
            if (retryCount == MAXIMUM_RETRIES) {
                // Poll view server on primary server address
                send(new GetView(), viewServer);
                retryCount = 0;
            }
            // Only resends to primaryServer if non-null
            if (primaryServer != null) {
                send(new Request(amoCommand), primaryServer);
            }
            set(t, CLIENT_RETRY_MILLIS);
        }
    }
}
