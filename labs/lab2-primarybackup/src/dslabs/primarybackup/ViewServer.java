package dslabs.primarybackup;

import com.google.common.base.Objects;
import dslabs.framework.Address;
import dslabs.framework.Node;
import java.util.HashSet;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static dslabs.primarybackup.PingCheckTimer.PING_CHECK_MILLIS;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
class ViewServer extends Node {
    static final int STARTUP_VIEWNUM = 0;
    private static final int INITIAL_VIEWNUM = 1;

    // Your code here...
    private int currViewNum = INITIAL_VIEWNUM;
    private View currView = new View(STARTUP_VIEWNUM, null, null);
    private boolean isViewAcked = false;
    private final HashSet<Address> secondLastPingServers = new HashSet<>();
    private final HashSet<Address> lastPingServers = new HashSet<>();
    /* -------------------------------------------------------------------------
        Construction and Initialization
       -----------------------------------------------------------------------*/
    public ViewServer(Address address) {
        super(address);
    }

    @Override
    public void init() {
        set(new PingCheckTimer(), PING_CHECK_MILLIS);
        // Your code here...
    }

    /* -------------------------------------------------------------------------
        Message Handlers
       -----------------------------------------------------------------------*/
    private void handlePing(Ping m, Address sender) {
        // Your code here...
        lastPingServers.add(sender);

        if (isFirstPing(m)) {
            currView = new View(currViewNum++, sender, null);
            isViewAcked = false;
            send(new ViewReply(currView), sender);
            return;
        }

        if (isPrimaryAck(m, sender)) {
            isViewAcked = true;
        }
        // Note: currView is non-null from here onwards
        if (isViewAcked && isBackupNull()) {
            // Assign a backup if available
            Address backup = getBackup();
            if (backup != null) {
                currView = new View(currViewNum++, currView.primary(), backup);
                isViewAcked = false;
            }
        }

        send(new ViewReply(currView), sender);
    }

    private void handleGetView(GetView m, Address sender) {
        // Your code here...
        send(new ViewReply(currView), sender);
    }

    /* -------------------------------------------------------------------------
        Timer Handlers
       -----------------------------------------------------------------------*/
    private void onPingCheckTimer(PingCheckTimer t) {
        // Your code here...
        set(t, PING_CHECK_MILLIS);
        // Figure out which servers have crashed
        Set<Address> crashedServers = new HashSet<>();
        crashedServers.addAll(secondLastPingServers);
        crashedServers.removeAll(lastPingServers);

        // Switch to new view if primary or backup has crashed
        if (isViewAcked) {
            if (crashedServers.contains(currView.primary()) && !isBackupNull()) {
                currView = new View(currViewNum++, currView.backup(), getBackup());
                isViewAcked = false;
            } else if (crashedServers.contains(currView.backup())) {
                currView = new View(currViewNum++, currView.primary(), getBackup());
                isViewAcked = false;
            }
        }

        // Reassign sets
        secondLastPingServers.clear();
        secondLastPingServers.addAll(lastPingServers);
        lastPingServers.clear();
    }

    /* -------------------------------------------------------------------------
        Utils
       -----------------------------------------------------------------------*/
    // Your code here...
    private boolean isPrimaryAck(Ping m, Address sender) {
        return !isViewNotStarted() && Objects.equal(currView.primary(), sender)
                && m.viewNum() == currView.viewNum();
    }

    private boolean isBackupNull() {
        return currView.backup() == null;
    }

    private boolean isViewNotStarted() {
        return currView.viewNum() == STARTUP_VIEWNUM;
    }

    private boolean isFirstPing(Ping m) {
        return m.viewNum() == STARTUP_VIEWNUM && isViewNotStarted();
    }

    private Address getBackup() {
        for (Address server : lastPingServers) {
            if (Objects.equal(server, currView.backup())
                || Objects.equal(server, currView.primary())) continue;

            return server;
        }

        return null;
    }
}
