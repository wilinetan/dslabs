package dslabs.primarybackup;

import com.google.common.base.Objects;
import dslabs.atmostonce.AMOApplication;
import dslabs.atmostonce.AMOCommand;
import dslabs.atmostonce.AMOResult;
import dslabs.framework.Address;
import dslabs.framework.Application;
import dslabs.framework.Node;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
class PBServer extends Node {
    private final Address viewServer;

    // Your code here...
    private AMOApplication app;
    private View currView = new View(ViewServer.STARTUP_VIEWNUM, null, null);
    private Request currRequest;
    /*
     Has 2 meanings depending on if server is the primary/backup or idle.
     If server is primary: means has state transfer completed
     If server is idle: means has state transfer been received
     */
    private boolean isStateTransferDone = true;

    /* -------------------------------------------------------------------------
        Construction and Initialization
       -----------------------------------------------------------------------*/
    PBServer(Address address, Address viewServer, Application app) {
        super(address);
        this.viewServer = viewServer;

        // Your code here...
        this.app = new AMOApplication<>(app);
    }

    @Override
    public void init() {
        // Your code here...
        send(new Ping(currView.viewNum()), viewServer);
        set(new PingTimer(), PingTimer.PING_MILLIS);
    }

    /* -------------------------------------------------------------------------
        Message Handlers
       -----------------------------------------------------------------------*/
    private void handleRequest(Request m, Address sender) {
        // Your code here...
        if (isPrimary() && isStateTransferDone && currRequest == null) {
            // Only can handle the request if server is primary
            if (currView.backup() == null || app.alreadyExecuted(m.command())) {
                // Will proceed to execute
                AMOResult result = executeCommand(m.command());
                send(new Reply(result), sender);
            } else {
                currRequest = m;
                forwardClientRequest(m);
            }
        }
    }

    private void handleViewReply(ViewReply m, Address sender) {
        // Your code here...
        if (m.view().viewNum() > currView.viewNum()) {
            // Need to update our view
            currView = m.view();
            if (isPrimary()) {
                // I am the new primary
                if (m.view().backup() != null) {
                    // Need to transfer state to new backup
                    this.isStateTransferDone = false;
                    sendStateTransfer(currView.backup());
                }
            } else if (isBackup()) {
                // Someone will be sending state to us
                this.isStateTransferDone = false;
            }
        }
    }

    // Your code here...
    // Sends client request to backup
    private void forwardClientRequest(Request m) {
        ForwardRequest forwardRequest = new ForwardRequest(m.command(), currView);
        send(forwardRequest, currView.backup());
        set(new ForwardRequestTimer(forwardRequest), ForwardRequestTimer.SERVER_RETRY_MILLIS);
    }

    // Backup receives forwarded request from primary
    private void handleForwardRequest(ForwardRequest f, Address sender) {
        if (!Objects.equal(currView, f.view())) return;
        if (isBackup() && currView.primary() == sender) {
            // Only execute if server is the backup
            executeCommand(f.command());
            send(new ForwardAck(f.command(), currView), sender);
        }
    }

    // Primary receives ack from backup
    private void handleForwardAck(ForwardAck f, Address sender) {
        /*
        If currRequest is null, means all requests have been processed already
        OR current request has been cancelled because of state transfer
         */
        if (currRequest == null) return;
        if (!Objects.equal(currView, f.view())) return;
        if (isPrimary() && Objects.equal(f.command(), currRequest.command())) {
            // Backup has acked the correct request so primary can run the command and send to client
            AMOResult result = executeCommand(f.command());
            send(new Reply(result), currRequest.command().address());
            currRequest = null;
        }
    }

    // Primary sends state to new backup
    private void sendStateTransfer(Address backup) {
        this.currRequest = null;
        StateTransfer stateTransfer = new StateTransfer(app, currView);
        send(stateTransfer, backup);
        set(new StateTransferTimer(stateTransfer), StateTransferTimer.SERVER_RETRY_MILLIS);
    }

    // Backup receives stateTransfer from primary
    private void handleStateTransfer(StateTransfer s, Address sender) {
        if (!Objects.equal(currView, s.view())) return;
        if (!isBackup()) return;
        if (!isStateTransferDone) {
            this.app = s.app();
            this.isStateTransferDone = true;
        }
        send(new TransferAck(currView), sender);
    }

    // Primary receives state transfer ACK
    private void handleTransferAck(TransferAck t, Address sender) {
        if (!Objects.equal(currView, t.view())) return;
        this.isStateTransferDone = true;
    }

    /* -------------------------------------------------------------------------
        Timer Handlers
       -----------------------------------------------------------------------*/
    private void onPingTimer(PingTimer t) {
        // Your code here...
        if (isStateTransferDone) {
            send(new Ping(currView.viewNum()), viewServer);
        } else {
            send(new Ping(currView.viewNum() - 1), viewServer);
        }
        set(t, PingTimer.PING_MILLIS);
    }
    // Your code here...
    // Backup did not ack in time
    private void onForwardRequestTimer(ForwardRequestTimer t) {
        /*
        If currRequest is null, means all requests have been processed already
        OR current request has been cancelled because of state transfer
         */
        if (currRequest == null) return;
        if (!Objects.equal(currView, t.request().view())) return;
        if (isPrimary() && currRequest.command().sequenceNum() == t.request().command().sequenceNum()) {
            send(t.request(), currView.backup());
            set(t, ForwardRequestTimer.SERVER_RETRY_MILLIS);
        }
    }

    private void onStateTransferTimer(StateTransferTimer t) {
        if (!Objects.equal(currView, t.stateTransfer().view())) return;
        if (!this.isStateTransferDone) {
            send(t.stateTransfer(), currView.backup());
            set(t, StateTransferTimer.SERVER_RETRY_MILLIS);
        }
    }

    /* -------------------------------------------------------------------------
        Utils
       -----------------------------------------------------------------------*/
    // Your code here...
    private boolean isPrimary() {
        return currView.primary() == this.address();
    }

    private boolean isBackup() {
        return currView.backup() == this.address();
    }

    private AMOResult executeCommand(AMOCommand command) {
        return app.execute(command);
    }
}
