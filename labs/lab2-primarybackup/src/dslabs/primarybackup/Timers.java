package dslabs.primarybackup;

import dslabs.atmostonce.AMOApplication;
import dslabs.primarybackup.Request;
import dslabs.framework.Timer;
import lombok.Data;

@Data
final class PingCheckTimer implements Timer {
    static final int PING_CHECK_MILLIS = 100;
}

@Data
final class PingTimer implements Timer {
    static final int PING_MILLIS = 25;
}

@Data
final class ClientTimer implements Timer {
    static final int CLIENT_RETRY_MILLIS = 75;

    // Your code here...
    private final Request request;
}

// Your code here...
@Data
final class StateTransferTimer implements Timer {
    static final int SERVER_RETRY_MILLIS = 60;

    private final StateTransfer stateTransfer;
}

@Data
final class ForwardRequestTimer implements Timer {
    static final int SERVER_RETRY_MILLIS = 60;

    // Your code here...
    private final ForwardRequest request;
}
