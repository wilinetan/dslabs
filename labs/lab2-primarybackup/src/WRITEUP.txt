Lab 2 Writeup

Partner 1 Name, NetID: Wu Peirong, peirong3
Partner 2 Name, NetID: Wiline Tan, wiline

Brief description of design:
In ViewServer, we only keep track of the current view and whether the primary has acked in the view so that we know
when we can transition into a new view. There is no need to store the previous view since we will not be using it
anymore once we want to go into a new view. In handlePing, we always add a server that has pinged into our set of
lastPingServers so that we know which servers are alive and are available to take over as primary backup if one of
them fails. We also do a check of whether the current view's primary has acked and whether there is currently a
backup. If the primary has already acked and there is no backup, we will try to find a backup for the next view.
Every onPingCheckTimer, we always start with whether the current view has been acked since we can only transition
to a new view after that. Then we check if primary or backup has crashed and update the view accordingly.

In PBClient, it follows closely to SimpleClient from lab1 except for the parts where we have to send a GetView
request to the ViewServer to get the current primary and to not send anything when there currently no PrimaryServer.
Another change is that we resend a new GetView request whenever we have failed to receive a reply for a request
3 times in a row.

In PBServer, in handleRequest, we only proccess the request from the client if we are the primary, state transfer to
backup is done and we are currently not processing any other request. Also, if we do not have a backup or we have
already executed the command before, we can just execute it without forwarding it to the backup. Otherwise, we forward
the request to the backup and wait for the ForwardAck from the backup. When a server receives a ForwardRequest, it
has to check that it is a forwarded request from the primary server of the same view otherwise it should
ignore it. When a server receives a ForwawrdAck, it has to check that it is currently processing a request,
check that it is for the current view and it is for the correct request before it can move on to execute the command
and send a Reply to the client. In handleViewReply, we only proces it if the view is newer than the one we have. Then,
we only need to check if we are the primary or the backup in the new view. If we are the new primary, we check if
there is a backup to send state to. If we are the new backup, we will set our isStateTransferDone to false since the
primary will be sending state to us. The new primary will send a StateTransfer message to the backup. When a server
receives a StateTransfer message, it has to check whether it is for the same view, it is the backup and it has not
finished state transfer since this may be a repeated message from the primary. When a server receives a TransferAck
message, it will check whether it is for the same view, if it is, we know that state transfer has been completed. In
onForwardRequestTimer, we only resend the ForwardRequest to the backup if we are still in the same view and it
is for the same command we are still processing. In onStateTransferTimer, we only resend it if we are still in the
same view and state transfer has not been completed.

Hours spent on the lab: 
20

Any feedback about the lab:
