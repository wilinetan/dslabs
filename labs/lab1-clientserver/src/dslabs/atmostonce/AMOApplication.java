package dslabs.atmostonce;

import dslabs.framework.Address;
import dslabs.framework.Application;
import dslabs.framework.Command;
import dslabs.framework.Result;
import java.util.HashMap;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.tuple.Pair;

@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public final class AMOApplication<T extends Application>
        implements Application {
    @Getter @NonNull private final T application;
    // Your code here...
    private HashMap<Address, AMOResult> cachedResults = new HashMap<>();

    @Override
    public AMOResult execute(Command command) {
        if (!(command instanceof AMOCommand)) {
            throw new IllegalArgumentException();
        }
        // Your code here...
        AMOCommand amoCommand = (AMOCommand) command;
        Address sender = amoCommand.address();
        int currSeq = amoCommand.sequenceNum();

        if (!cachedResults.containsKey(sender) || cachedResults.get(sender).sequenceNum() < currSeq) {
            Result result = application.execute(amoCommand.command());
            AMOResult amoResult = new AMOResult(result, currSeq);
            cachedResults.put(sender, amoResult);
        }

        return cachedResults.get(sender);
    }

    public Result executeReadOnly(Command command) {
        if (!command.readOnly()) {
            throw new IllegalArgumentException();
        }

        if (command instanceof AMOCommand) {
            return execute(command);
        }

        return application.execute(command);
    }

    public boolean alreadyExecuted(AMOCommand amoCommand) {
        // Your code here...
        Address sender = amoCommand.address();
        return cachedResults.containsKey(sender) &&
                cachedResults.get(sender).sequenceNum() == amoCommand.sequenceNum();
    }
}
