package dslabs.paxos;

import dslabs.framework.Timer;
import lombok.Data;

@Data
final class ClientTimer implements Timer {
    static final int CLIENT_RETRY_MILLIS = 100;

    // Your code here...
    private final PaxosRequest request;
}

@Data
final class HeartBeatTimer implements Timer {
    static final int RETRY_MILLIS = 100;

    // Your code here...
}

// Your code here...
@Data
final class P1AMessageTimer implements Timer {
    static final int RETRY_MILLIS = 100;

    // Your code here...
    private final P1AMessage message;
}
