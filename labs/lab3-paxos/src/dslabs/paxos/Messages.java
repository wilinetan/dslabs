package dslabs.paxos;

import dslabs.atmostonce.AMOCommand;
import dslabs.framework.Address;
import dslabs.framework.Message;
import java.util.Map;
import lombok.Data;

// Your code here...
@Data
class HeartBeatMessage implements Message {
    private final Ballot leaderBallot;
    private final int slotIn;
    private final int slotOut;
    private final int slotClear;
    private final Map<Integer, LogEntry> log;
}

@Data
class P1AMessage implements Message {
    private final Ballot ballot;
}

@Data
class P1BMessage implements Message {
    private final Ballot ballot;
    private final Map<Integer, LogEntry> log;
    private final int slotOut;
}

@Data
class P2AMessage implements Message {
    private final Integer slot;
    private final LogEntry entry;
}

@Data
class P2BMessage implements Message {
    private final Integer slot;
    private final LogEntry entry;
    private final int slotOut;
}
