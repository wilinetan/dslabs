package dslabs.paxos;

public enum PaxosServerStatus {
    LEADER_FOUND,
    LEADER_ELECTION
}
