package dslabs.paxos;

import dslabs.framework.Address;
import lombok.Data;

@Data
public final class Ballot implements Address {
    private final int round;
    private final Address address;

    @Override
    public int compareTo(Address o) {
        if (!(o instanceof Ballot)) return -1;
        Ballot other = (Ballot) o;

        return round() == other.round()
                ? address().compareTo(other.address())
                : round() - other.round();
    }
}
