package dslabs.paxos;

import dslabs.atmostonce.AMOApplication;
import dslabs.atmostonce.AMOCommand;
import dslabs.atmostonce.AMOResult;
import dslabs.framework.Address;
import dslabs.framework.Application;
import dslabs.framework.Command;
import dslabs.framework.Node;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/*
General Notes:
Right now before we send a P2A, we put the entry into the leader's log first
because we are scared the leader will receive a P2B from some other server before
it has received its own P2A (which will place the entry into its log). Moving forward,
we can remove this and see if it still works if we DON'T put the entry into the leader's
log before P2A.
 */

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PaxosServer extends Node {
    /** All servers in the Paxos group, including this one. */
    private final Address[] servers;

    private AMOApplication app;

    // General server fields
    Map<Integer, LogEntry> log;
    Ballot myBallot;
    // candidate or actual leader
    Ballot highestProposedBallot;
    PaxosServerStatus status;

    // ACCEPTOR FIELDS
    // some fields to keep track of whether leader is down since last timer
    boolean isLeaderUp;
    int numDownCycles;

    // DISTINGUISHED PROPOSER FIELDS
    // keep count of total number of servers that acknowledged P1A
    Set<Address> p1bServers;
    Map<Integer, Set<Address>> p2bResponses;
    Map<Integer, Set<Address>> executedSlots;
    // slotIn points to the first empty slot AFTER a filled slot
    int slotIn;
    // slotOut points to the first hole
    int slotOut;
    // slotClear points to the first non garbage collected slot
    int slotClear;

    /*
     * -------------------------------------------------------------------------
     * Construction and Initialization
     * -----------------------------------------------------------------------
     */
    public PaxosServer(Address address, Address[] servers, Application app) {
        super(address);
        this.servers = servers;
        this.app = new AMOApplication<>(app);
        this.log = new HashMap<>();
        this.myBallot = new Ballot(0, address());
        this.highestProposedBallot = new Ballot(0, address());
        this.isLeaderUp = false;
        this.numDownCycles = 0;
        this.p2bResponses = new HashMap<>();
        this.executedSlots = new HashMap<>();
        this.p1bServers = new HashSet<>();
        this.slotIn = 1;
        this.slotOut = 1;
        this.slotClear = 1;
    }

    @Override
    public void init() {
        tryForLeader();
        set(new HeartBeatTimer(), HeartBeatTimer.RETRY_MILLIS);
    }

    /*
     * -------------------------------------------------------------------------
     * Interface Methods
     * 
     * Be sure to implement the following methods correctly. The test code uses
     * them to check correctness more efficiently.
     * -----------------------------------------------------------------------
     */

    /**
     * Return the status of a given slot in the server's local log.
     *
     * If this server has garbage-collected this slot, it should return {@link
     * PaxosLogSlotStatus#CLEARED} even if it has previously accepted or chosen
     * command for this slot. If this server has both accepted and chosen a
     * command for this slot, it should return {@link PaxosLogSlotStatus#CHOSEN}.
     *
     * Log slots are numbered starting with 1.
     *
     * @param logSlotNum
     *                   the index of the log slot
     * @return the slot's status
     *
     * @see PaxosLogSlotStatus
     */
    public PaxosLogSlotStatus status(int logSlotNum) {
        if (logSlotNum < slotClear) {
            return PaxosLogSlotStatus.CLEARED;
        } else if (log.containsKey(logSlotNum)) {
            return log.get(logSlotNum).status();
        } else {
            return PaxosLogSlotStatus.EMPTY;
        }
    }

    /**
     * Return the command associated with a given slot in the server's local
     * log.
     *
     * If the slot has status {@link PaxosLogSlotStatus#CLEARED} or {@link
     * PaxosLogSlotStatus#EMPTY}, this method should return {@code null}.
     * Otherwise, return the command this server has chosen or accepted,
     * according to {@link PaxosServer#status}.
     *
     * If clients wrapped commands in {@link dslabs.atmostonce.AMOCommand}, this
     * method should unwrap them before returning.
     *
     * Log slots are numbered starting with 1.
     *
     * @param logSlotNum
     *                   the index of the log slot
     * @return the slot's contents or {@code null}
     *
     * @see PaxosLogSlotStatus
     */
    public Command command(int logSlotNum) {
        PaxosLogSlotStatus slotStatus = status(logSlotNum);
        if (slotStatus == PaxosLogSlotStatus.CLEARED
                || slotStatus == PaxosLogSlotStatus.EMPTY) {
            return null;
        } else {
            return log.get(logSlotNum).command().command();
        }
    }

    /**
     * Return the index of the first non-cleared slot in the server's local log.
     * The first non-cleared slot is the first slot which has not yet been
     * garbage-collected. By default, the first non-cleared slot is 1.
     *
     * Log slots are numbered starting with 1.
     *
     * @return the index in the log
     *
     * @see PaxosLogSlotStatus
     */
    public int firstNonCleared() {
        return slotClear;
    }

    /**
     * Return the index of the last non-empty slot in the server's local log,
     * according to the defined states in {@link PaxosLogSlotStatus}. If there
     * are no non-empty slots in the log, this method should return 0.
     *
     * Log slots are numbered starting with 1.
     *
     * @return the index in the log
     *
     * @see PaxosLogSlotStatus
     */
    public int lastNonEmpty() {
        return slotIn - 1;
    }

    /*
     * -------------------------------------------------------------------------
     * Message Handlers
     * -----------------------------------------------------------------------
     */
    private void handlePaxosRequest(PaxosRequest m, Address sender) {
        if (!isLeader() || status == PaxosServerStatus.LEADER_ELECTION) return;
        if (app.alreadyExecuted(m.command())) {
            AMOResult result = app.execute(m.command());
            send(new PaxosReply(result), sender);
            return;
        }
        // check if this command is already in the logs
        for (int slot = slotOut; slot < slotIn; slot++) {
            if (status(slot) == PaxosLogSlotStatus.EMPTY) continue;

            LogEntry entry = log.get(slot);
            if (entry.command().equals(m.command())) {
                // we are in the midst of getting this command chosen and executed
                return;
            }
        }
        /* we only reach here if we as the leader cannot find any existing
        entry with the command inside
         */
        proposeCommand(slotIn++, m.command());
    }

    private void handleP1AMessage(P1AMessage message, Address sender) {
        // we only reply to P1A if sender deserves to be the new leader
        if (getOrder(message.ballot(), highestProposedBallot) == BallotOrder.SMALLER) return;
        highestProposedBallot = message.ballot();
        /* we may be in the case where we are already the leader
        (not just in the middle of election) but we are pre-empted
        by someone with a higher ballot.
         */
        status = PaxosServerStatus.LEADER_ELECTION;
        /* Need to clear regardless when we enter P1 as we may have been in
        the process of trying to be leader before we received this P1A
         */
        p1bServers.clear();
        // reply with P1B
        P1BMessage replyMessage = new P1BMessage(highestProposedBallot, log, slotOut);
        if (sender.equals(address())) {
            handleMessage(replyMessage);
        } else {
            send(replyMessage, sender);
        }
    }

    private void handleP1BMessage(P1BMessage message, Address sender) {
        // we ignore all P1Bs when we are elected already
//        if (status == PaxosServerStatus.LEADER_FOUND) return;
        /* we could have been pre-empted by another server with a higher ballot which
        led to a higher leaderBallot. in this case, we ignore all P2B destined for us
         */
        if (!isLeader()) return;
        /* we now know we are the
        highest proposal ballot, so we need to check
        if that message is indeed for us. the message could also be destined
        for the older version of us
         */
        if (getOrder(myBallot, message.ballot()) != BallotOrder.EQUAL) return;

        /* for all slots greater than or equal to our slotClear and smaller than
        the sender's slotOut, we know that the sender has already executed it and
        we have not garbage collected the slot
         */
        for (int i = slotClear; i < message.slotOut(); i++) {
            executedSlots.putIfAbsent(i, new HashSet<>());
            executedSlots.get(i).add(sender);
        }
        garbageCollect();

        // we have already been elected as the leader
        if (status == PaxosServerStatus.LEADER_FOUND) return;

        p1bServers.add(sender);

        // if P1B is a vote of confidence in your election
        for (int slot : message.log().keySet()) {
            if (slot < slotOut) continue;
            /* move our slotIn to the most RHS slot based on where other
                server's slots are occupied
             */

            slotIn = Math.max(slotIn, slot + 1);
            /* if we reach here, the slot is >= slotOut. this means there will
            be no entries of status CLEARED
             */
            LogEntry newEntry = message.log().get(slot);
            log.putIfAbsent(slot, newEntry);
            LogEntry oldEntry = log.get(slot);
            BallotOrder order = getOrder(newEntry.ballot(), oldEntry.ballot());
            // assuming here that whoever has the higher ballot is the correct entry
            switch (order) {
                case EQUAL:
                    if (oldEntry.status() == PaxosLogSlotStatus.CHOSEN) {
                        newEntry = oldEntry;
                    }
                    break;
                case SMALLER:
                    newEntry = oldEntry;
                    break;
                default:
                    break;
            }
            log.put(slot, newEntry);
        }

        if (isMajorityP1BAcked()) {
            /* i am now leader, so we send out P2A for logs with status that are
            ACCEPTED
             */
            status = PaxosServerStatus.LEADER_FOUND;
            p1bServers.clear();

            for (int slot = slotOut; slot < slotIn; slot++) {
                PaxosLogSlotStatus slotStatus = status(slot);
                // we are at a non-hole
                if (slotStatus == PaxosLogSlotStatus.EMPTY) {
                    // we need to propose a no-op at this slot because there is no entry
                    proposeCommand(slot, new PaxosNoOpCommand());
                } else if (slotStatus == PaxosLogSlotStatus.ACCEPTED) {
                    // we need to re-propose the value at this slot with your OWN ballot
                    proposeCommand(slot, log.get(slot).command());
                }
            }
            // try to advance slotOut as far as possible now more slots should have been filled
            executeLogs();
        }
    }

    private void handleP2AMessage(P2AMessage message, Address sender) {
        LogEntry p2AEntry = message.entry();
        Ballot p2ABallot = p2AEntry.ballot();
        if (getOrder(p2ABallot, highestProposedBallot) == BallotOrder.SMALLER) return;
        /* set highest proposed ballot to proposer. if there are 2 different proposers,
        the one with the lower ballot will realise he is not the leader after receiving
        HeartBeatMessage from the higher ballot proposer
         */
        highestProposedBallot = p2ABallot;
        status = PaxosServerStatus.LEADER_FOUND;
        isLeaderUp = true;

        int slot = message.slot();
        PaxosLogSlotStatus slotStatus = status(slot);

        if (slotStatus == PaxosLogSlotStatus.CHOSEN ||
        slotStatus == PaxosLogSlotStatus.CLEARED) return;
        // we need to check if current slot ballot > P2A ballot
        if (slotStatus == PaxosLogSlotStatus.ACCEPTED &&
        getOrder(log.get(slot).ballot(), p2ABallot) == BallotOrder.GREATER) return;
        /* we know we won't see a P2A with a smaller ballot. this is because
        we checked P2A ballot is the highest we have seen so far AND also
        higher than the current entry (if it exists). the
        ballot of the slot it is proposing should match that as well.
         */
        log.put(slot, p2AEntry);
        /* we need to increment slotIn if the slot we are putting in is larger
        than our current slotIn
         */
        slotIn = Math.max(slotIn, slot + 1);
        // send P2B message back to leader
        P2BMessage p2BMessage = new P2BMessage(slot, p2AEntry, slotOut);
        if (isLeader()) {
            handleMessage(p2BMessage);
        } else {
            send(p2BMessage, sender);
        }

    }

    private void handleP2BMessage(P2BMessage message, Address sender) {
        LogEntry p2BEntry = message.entry();
        Ballot p2BBallot = p2BEntry.ballot();
        int slot = message.slot();
        // Could be a delayed P2B for when we were a leader in the past
        if (!isLeader()) return;
        if (getOrder(p2BBallot, myBallot) != BallotOrder.EQUAL) return;

        /* for all slots greater than or equal to our slotClear and smaller than
        the sender's slotOut, we know that the sender has already executed it and
        we have not garbage collected the slot
         */
        for (int i = slotClear; i < message.slotOut(); i++) {
            executedSlots.putIfAbsent(i, new HashSet<>());
            executedSlots.get(i).add(sender);
        }
        garbageCollect();

        // we have already received a majority of P2Bs for this slot
        if (status(slot) == PaxosLogSlotStatus.CHOSEN) return;

        if (!p2bResponses.containsKey(slot)) {
            p2bResponses.put(slot, new HashSet<>());
        }

        p2bResponses.get(slot).add(sender);

        /* check if majority has been reached, if so change status of LogEntry
        from ACCEPTED to CHOSEN
         */
        if (isMajorityP2BAcked(slot)) {
            // we need to clear all P2Bs if not we won't know if this slot is CHOSEN yet
            p2bResponses.remove(slot);
            LogEntry chosenEntry = new LogEntry(p2BEntry.ballot(),
                    PaxosLogSlotStatus.CHOSEN, p2BEntry.command());
            log.put(slot, chosenEntry);
            // execute logs until first hole or until slotIn reached
            executeLogs();
        }
    }

    private void handleHeartBeatMessage(HeartBeatMessage message, Address sender) {
        /* HeartBeatMessage is the only reliable way (P2As aren't reliable as they
        may not be sent even when a leader exists) for other servers to know a
        leader is elected.
         */
        if (getOrder(message.leaderBallot(), highestProposedBallot) == BallotOrder.SMALLER) return;
        isLeaderUp = true;
        status = PaxosServerStatus.LEADER_FOUND;
        highestProposedBallot = message.leaderBallot();
        slotIn = Math.max(slotIn, message.slotIn());

        for (int slot : message.log().keySet()) {
            if (slot < slotOut) continue;
            /* if we reach here, the slot is >= slotOut. this means there will
            be no entries of status CLEARED
             */
            LogEntry newEntry = message.log().get(slot);
            log.putIfAbsent(slot, newEntry);
            LogEntry oldEntry = log.get(slot);
            /* as long as leader's slot ballot is >= than ours, we take leader's
            because leader may have updated its slot status from ACCEPTED to CHOSEN
             */
            LogEntry finalEntry =
                    getOrder(newEntry.ballot(), oldEntry.ballot()) != BallotOrder.SMALLER
                            ? newEntry : oldEntry;
            log.put(slot, finalEntry);
        }
        executeLogs();
        garbageCollect(message.slotClear());
    }

    /*
     * -------------------------------------------------------------------------
     * Timer Handlers
     * -----------------------------------------------------------------------
     */
    private void onP1AMessageTimer(P1AMessageTimer t) {
        if (status == PaxosServerStatus.LEADER_ELECTION
                && getOrder(myBallot, highestProposedBallot) == BallotOrder.EQUAL) {
            tryForLeader();
        }
    }

    private void onHeartBeatTimer(HeartBeatTimer t) {
        if (status == PaxosServerStatus.LEADER_ELECTION) {
            // we should not do anything if we are still in election as we dk who is the leader yet
            numDownCycles++;
            if (numDownCycles == 3) {
                tryForLeader();
            }
        } else if (isLeader()) {
            // Check if there are any empty slots, if there are, we have to propose no-op for that slot
            for (int slot = slotOut; slot < slotIn; slot++) {
                PaxosLogSlotStatus slotStatus = status(slot);
                if (slotStatus == PaxosLogSlotStatus.EMPTY) {
                    // propose no-op for this slot
                    proposeCommand(slot, new PaxosNoOpCommand());
                } else if (slotStatus == PaxosLogSlotStatus.ACCEPTED) {
                    proposeCommand(slot, log.get(slot).command());
                }
            }
            HeartBeatMessage heartBeatMessage = new HeartBeatMessage(myBallot, slotIn, slotOut, slotClear, log);
            for (Address server : servers) {
                if (server.equals(address())) {
                    handleMessage(heartBeatMessage);
                } else {
                    send(heartBeatMessage, server);
                }
            }
        } else if (!isLeaderUp) {
            // we are an acceptor and we detected the leader is down
            numDownCycles++;
            if (numDownCycles == 3) {
                tryForLeader();
            }
        } else {
            isLeaderUp = false;
            numDownCycles = 0;
        }
        set(t, HeartBeatTimer.RETRY_MILLIS);
    }

    /*
     * -------------------------------------------------------------------------
     * Utils
     * -----------------------------------------------------------------------
     */
    // NOTE: P1As are sent out and P1A timer is set in this function!
    private final void tryForLeader() {
        int newRoundNum = highestProposedBallot.round() + 1;
        // reset so we can try for leader
        p1bServers.clear();
        p2bResponses.clear();
        executedSlots.clear();
        status = PaxosServerStatus.LEADER_ELECTION;
        numDownCycles = 0;
        // send out P1As to everyone
        myBallot = new Ballot(newRoundNum, address());
        P1AMessage message = new P1AMessage(myBallot);
        for (Address server : servers) {
            if (server.equals(address())) {
                handleMessage(message);
            } else {
                send(message, server);
            }
        }
        set(new P1AMessageTimer(message), P1AMessageTimer.RETRY_MILLIS);
    }

    // Send out proposal for one log slot
    private final void proposeCommand(int slot, AMOCommand command) {
        LogEntry entry = new LogEntry(myBallot, PaxosLogSlotStatus.ACCEPTED, command);
        P2AMessage message = new P2AMessage(slot, entry);

        for (Address server : servers) {
            if (server.equals(address())) {
                handleMessage(message);
            } else {
                send(message, server);
            }
        }
    }

    /* execute logs from slotOut as far as possible or until slotIn reached
    and send responses to client!
     */
    private final void executeLogs() {
        while (slotOut < slotIn) {
            PaxosLogSlotStatus logStatus = status(slotOut);
            // we only want to advance slotOut if slots are CHOSEN
            if (logStatus == PaxosLogSlotStatus.EMPTY
            || logStatus == PaxosLogSlotStatus.ACCEPTED) break;

            AMOCommand command = log.get(slotOut).command();

            // we do not execute when it is no-op
            if (command instanceof PaxosNoOpCommand) {
                slotOut++;
                continue;
            }

            AMOResult result = app.execute(command);
            slotOut++;
            if (isLeader()) {
                send(new PaxosReply(result), command.address());
            }
        }
    }

    /* garbage collect logs for leader from slotClear as far as possible until slotOut
    slotClear <= slotOut <= slotIn
     */
    private final void garbageCollect() {
        while (slotClear < slotOut) {
            if (!executedSlots.containsKey(slotClear)
                    || executedSlots.get(slotClear).size() < servers.length) {
                break;
            }
            log.remove(slotClear);
            executedSlots.remove(slotClear);
            slotClear++;
        }
    }

    /* garbage collect for other servers until leader's slotClear
     */
    private final void garbageCollect(int leaderSlotClear) {
        while (slotClear < leaderSlotClear) {
            log.remove(slotClear);
            slotClear++;
        }
    }

    // Check for whether there is a leader candidate
    private final boolean isLeader() {
        return getOrder(highestProposedBallot, myBallot) == BallotOrder.EQUAL;
    }

    private final int getMajoritySize() {
        return (int) Math.floor(servers.length / 2);
    }

    private final boolean isMajorityP1BAcked() {
        return p1bServers.size() > getMajoritySize();
    }

    private final boolean isMajorityP2BAcked(int slot) {
        return p2bResponses.get(slot).size() > getMajoritySize();
    }
    // returns an Enum denoting if first ballot's order with respect to second ballot
    private final BallotOrder getOrder(Address first, Address second) {
        if (first.compareTo(second) < 0) {
            return BallotOrder.SMALLER;
        } else if (first.compareTo(second) == 0) {
            return BallotOrder.EQUAL;
        } else {
            return BallotOrder.GREATER;
        }
    }
}
