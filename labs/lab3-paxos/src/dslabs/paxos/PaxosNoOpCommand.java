package dslabs.paxos;

import dslabs.atmostonce.AMOCommand;

public class PaxosNoOpCommand extends AMOCommand {
    public PaxosNoOpCommand() {
        super(null, null, -1);
    }
}
