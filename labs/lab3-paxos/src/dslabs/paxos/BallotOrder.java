package dslabs.paxos;

public enum BallotOrder {
    SMALLER,
    EQUAL,
    GREATER
}
