package dslabs.paxos;

import com.google.common.base.Objects;
import dslabs.atmostonce.AMOCommand;
import dslabs.atmostonce.AMOResult;
import dslabs.framework.Address;
import dslabs.framework.Client;
import dslabs.framework.Command;
import dslabs.framework.Node;
import dslabs.framework.Result;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class PaxosClient extends Node implements Client {
    private final Address[] servers;

    // Your code here...
    private int currSeq = 0;
    private AMOCommand amoCommand;
    private AMOResult amoResult;

    /* -------------------------------------------------------------------------
        Construction and Initialization
       -----------------------------------------------------------------------*/
    public PaxosClient(Address address, Address[] servers) {
        super(address);
        this.servers = servers;
    }

    @Override
    public synchronized void init() {
        // No need to initialize
    }

    /* -------------------------------------------------------------------------
        Public methods
       -----------------------------------------------------------------------*/
    @Override
    public synchronized void sendCommand(Command operation) {
        // Your code here...
        AMOCommand amoCommand = new AMOCommand(operation, address(), currSeq);
        PaxosRequest request = new PaxosRequest(amoCommand);
        this.amoCommand = amoCommand;
        amoResult = null;

        for (Address server: servers) {
            send(request, server);
        }

        set(new ClientTimer(request), ClientTimer.CLIENT_RETRY_MILLIS);
    }

    @Override
    public synchronized boolean hasResult() {
        // Your code here...
        return amoResult != null;
    }

    @Override
    public synchronized Result getResult() throws InterruptedException {
        // Your code here...
        while (amoResult == null) {
            wait();
        }
        return amoResult.result();
    }

    /* -------------------------------------------------------------------------
        Message Handlers
       -----------------------------------------------------------------------*/
    private synchronized void handlePaxosReply(PaxosReply m, Address sender) {
        // Your code here...
        if (Objects.equal(m.result().sequenceNum(), currSeq) && amoResult == null) {
            currSeq++;
            amoResult = m.result();
            notify();
        }
    }

    /* -------------------------------------------------------------------------
        Timer Handlers
       -----------------------------------------------------------------------*/
    private synchronized void onClientTimer(ClientTimer t) {
        // Your code here...
        AMOCommand timerCommand = t.request().command();
        if (amoCommand.sequenceNum() == timerCommand.sequenceNum()
                && amoResult == null) {
            for (Address server: servers) {
                send(new PaxosRequest(amoCommand), server);
            }

            set(t, ClientTimer.CLIENT_RETRY_MILLIS);
        }
    }
}
