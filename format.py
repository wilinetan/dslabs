#!/usr/bin/env python3

# Simple script to pretty-print parenthesized data.
#
# Usage: 
#     format.py FILENAME
#
# Formats the file FILENAME and prints the result to stdout.
#
# Requires a fairly modern version of python, I tested on 3.9, but probably 3.7 would work too.
#
# Author: James Wilcox


from dataclasses import dataclass
import sys
from typing import cast, List, Optional, Union

DEBUG = False

opens = '([{'
closes = ')]}'
groupers = opens + closes
seps = ',\n'

Node = Union[str, "ListNode"]


@dataclass
class ListNode:
    prefix: str
    elts: List[Node]
    suffix: str
    normalized = False

    def append(self, n: Node) -> None:
        self.elts.append(n)


def normalize_children(n: ListNode) -> None:
    if n.normalized:
        return

    if n.elts and DEBUG:
        for c in n.elts:
            if isinstance(c, str):
                print(c, end=' ')
            else:
                print('listnode', end=' ')
        print()

    children: List[Node] = []
    current: Optional[str] = None
    for c in n.elts:
        if isinstance(c, str) and c not in seps:
            if isinstance(current, str):
                current += c
            else:
                current = c
        else:
            if isinstance(c, str) and c in seps:
                if current is not None:
                    current += c
                else:
                    if c != '\n':
                        current = c

            if isinstance(current, str):
                children.append(current.strip())
                current = None
            if not (isinstance(c, str) and c in seps):
                children.append(c)
    if isinstance(current, str):
        children.append(current.strip())
        current = None

    if children and DEBUG:
        for c in children:
            if isinstance(c, str):
                print(c, end=' ')
            else:
                print('listnode', end=' ')
        print()

    children2: List[Node] = []
    i = 0
    while i < len(children):
        c = children[i]
        if isinstance(c, ListNode) and i > 0 and \
           isinstance(children[i-1], str) and \
           any(map(lambda x: cast(str, children[i-1]).endswith(x), opens)) and \
           i + 1 < len(children) and \
           isinstance(children[i+1], str) and \
           any(map(lambda x: cast(str, children[i+1]).startswith(x), closes)):
            assert children2[-1] == children[i-1]
            children2.pop()
            if c.prefix != '' or c.suffix != '':
                assert False
            c.prefix = cast(str, children[i-1])
            c.suffix = cast(str, children[i+1])
            children2.append(c)
            i += 1
        else:
            children2.append(c)
        i += 1

    if children2 and DEBUG:
        for c in children2:
            if isinstance(c, str):
                print(c, end=' ')
            elif c.prefix != '' or c.suffix != '':
                print(f'listnode(prefix={repr(c.prefix)}, suffix={repr(c.suffix)})', end=' ')
            else:
                print('listnode', end=' ')
        print()

    n.elts = children2
    n.normalized = True


def node_on_one_line(n: Node):
    if isinstance(n, str):
        return n
    else:
        normalize_children(n)
        result = []
        result.append(n.prefix)
        for c in n.elts:
            if isinstance(c, str):
                result.append(c)
                if c.endswith(','):
                    result.append(' ')
            else:
                result.append(node_on_one_line(c))
        result.append(n.suffix)
        return ''.join(result)


MAX_WIDTH = 120


def node_lines(n: Node, indent: int, lines: List[str]):
    nool = node_on_one_line(n)
    if 2 * indent + len(nool) <= MAX_WIDTH:
        lines.append((2 * indent * ' ') + nool)
        return

    if isinstance(n, str):
        lines.append(2 * indent * ' ' + n)
    else:
        normalize_children(n)
        if n.prefix != '':
            assert n.suffix != ''
            lines.append(2 * indent * ' ' + n.prefix)
        else:
            assert n.suffix == ''

        new_indent = indent if len(lines) == 0 else indent+1
        for c in n.elts:
            node_lines(c, new_indent, lines)
        if n.suffix != '':
            lines.append(2 * indent * ' ' + n.suffix)


def main() -> None:
    if len(sys.argv) != 2:
        print('need arg')
        sys.exit(1)

    with open(sys.argv[1]) as f:
        s = f.read()
    tree = [ListNode('', [], '')]
    for c in s:
        if c in opens:
            tree[-1].append(c)
            tree.append(ListNode('', [], ''))
        elif c in closes:
            last = tree.pop()
            tree[-1].append(last)
            tree[-1].append(c)
        else:
            tree[-1].append(c)

    root = ListNode('', list(tree), '')
    lines: List[str] = []
    node_lines(root, 0, lines)
    print('\n'.join(lines))


if __name__ == '__main__':
    main()
 